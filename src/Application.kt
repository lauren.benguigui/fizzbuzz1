package ktor.benguigui

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.html.*
import kotlinx.html.*
import kotlinx.css.*
import freemarker.cache.*
import io.ktor.freemarker.*
import io.ktor.http.content.*
import io.ktor.sessions.*

// Algorithm FizzBuzz
fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)
fun FizzBuzz(numbersOfTurns: Int) : String {
    var Result = ""

    for (i in 1..numbersOfTurns){
        if (i % 3 == 0){
            Result = "Fizz"
        }
        if (i % 5 == 0){
            Result = "Buzz"
        }
        if (i % 5 == 0 && i % 3 == 0){
            Result = "FizzBuzz"
        }
    }
    return(Result)
}


@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(FreeMarker) {
        templateLoader = ClassTemplateLoader(this::class.java.classLoader, "templates")
    }

    install(Sessions) {
        cookie<MySession>("MY_SESSION") {
            cookie.extensions["SameSite"] = "lax"
        }
    }

    routing {
        get("/") {
            call.respondText("FizzBuzz", contentType = ContentType.Text.Plain)
        }

        get("/fizzbuzz") {
            call.respondHtml {
                body {
                    h1 { +"FizzBuzz" }
                    p { +"Si le nombre est divisble par 3, on affiche Fizz."}
                    p { +"Si le nombre est divisble par 5, on affiche Buzz."}
                    p { +"Si le nombre est divisble par 3 et par 5, on affiche FizzBuzz."}
                    h2 { +"TestFizzBuzz:" }
                    form("/response", method = FormMethod.post){
                        div {
                            label { +"Choisissez un nombre:" }
                            textInput {
                                type = InputType.number
                                id = "number"
                                name = "number"
                                button {
                                    type = ButtonType.submit
                                }
                            }
                        }
                    }
                }
            }
        }

        post("/response"){
            var Number = call.receiveParameters()["number"] // recupère les informations
            val TheNumber = Number?.toIntOrNull() // retourne le resultat ou affiche null
            if (TheNumber != null){
                var Result = FizzBuzz(TheNumber) // algorithm FizzBuzz
                call.respondHtml{
                    body{
                        h1{+"Résultat:"}
                        p{+"Le nombre que je choisi est $TheNumber."}
                        p{+"Le résultat est $Result."}
                    }
                }
            }
        }




            get("/styles.css") {
            call.respondCss {
                body {
                    backgroundColor = Color.red
                }
                p {
                    fontSize = 2.em
                }
                rule("p.myclass") {
                    color = Color.blue
                }
            }
        }

        get("/html-freemarker") {
            call.respond(FreeMarkerContent("index.ftl", mapOf("data" to IndexData(listOf(1, 2, 3))), ""))
        }




        // Static feature. Try to access `/static/ktor_logo.svg`
        static("/static") {
            resources("static")
        }

        get("/session/increment") {
            val session = call.sessions.get<MySession>() ?: MySession()
            call.sessions.set(session.copy(count = session.count + 1))
            call.respondText("Counter is ${session.count}. Refresh to increment.")
        }
    }
}

data class IndexData(val items: List<Int>)

data class MySession(val count: Int = 0)

fun FlowOrMetaDataContent.styleCss(builder: CSSBuilder.() -> Unit) {
    style(type = ContentType.Text.CSS.toString()) {
        +CSSBuilder().apply(builder).toString()
    }
}

fun CommonAttributeGroupFacade.style(builder: CSSBuilder.() -> Unit) {
    this.style = CSSBuilder().apply(builder).toString().trim()
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}
